﻿using System;
using System.Collections;

namespace library
{
    public class BooksList : IEnumerable, IEnumerator
    {
        public Book[] books;
        public int Count
        {
            get;
            private set;
        }
        public int Capacity
        {
            get { return books.Length; }
        }

        public BooksList()
        {
            Count = 0;
            books = new Book[4];
        }

        public int Add(Book book)
        {
            books[Count] = book;
            return Count++;
        }

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        #endregion

        #region IEnumerator

        int position = -1;

		object IEnumerator.Current
		{
			get { return books[position]; }
		}

        void IEnumerator.Reset()
        {
            position = -1;
        }

        bool IEnumerator.MoveNext()
        {
            if (position < Count - 1){
                position++;
                return true;
            }

            return false;
        }

        #endregion
    }
}
