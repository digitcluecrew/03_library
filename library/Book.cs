﻿using System;
namespace library
{
    public class Book
    {
        public int Id
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public Author BookAuthor
        {
            get;
            set;
        }
        public int Year
        {
            get;
            set;
        }
        public int PagesCount
        {
            get;
            set;
        }
        public string Publisher
        {
            get;
            set;
        }
        public string Genre
        {
            get;
            set;
        }
        public Book(int id, string name, Author author, int year, int pagesCount, string publisher, string genre)
        {
            Id = id;
            Name = name;
            BookAuthor = author;
            Year = year;
            PagesCount = pagesCount;
            Publisher = publisher;
            Genre = genre;
        }
    }
}
