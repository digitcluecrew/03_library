﻿using System;

namespace library
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Library v0.1");
            Console.WriteLine("Type command. Use \"help\" to list available commands");
            Console.WriteLine();

            Author king = new Author("Stephen", "King", 70);

            Book book = new Book(1, "It", king, 1985, 700, "Trololo", "Horror");

            BooksList list = new BooksList();

            list.Add(book);

            foreach (Book b in list)
            {
				Console.WriteLine("Book: {0}", b.Name);
            }


            return;
            bool terminate = false;

            while (!terminate)
            {
                Console.Write(">");

                string action = Console.ReadLine();

                switch (action)
                {
                    case "ab":
                    case "add book":
                        AddBook();
                        break;
                    case "lb":
                    case "list books":
                        ListBooks();
                        break;
                    case "aa":
                    case "add author":
                        AddAuthor();
                        break;
                    case "la":
                    case "list authors":
                        ListAuthors();
                        break;
                    case "q":
                    case "quit":
                        Console.WriteLine("Goodbye!");
                        terminate = true;
                        break;
                    case "h":
                    case "help":
                        ShowHelp();
                        break;
                    default:
                        Console.WriteLine("Command \"{0}\" not found. Use \"help\" to list available commands", action);
                        break;
                }
            }
        }

        static void AddBook()
        {
            Console.WriteLine("Add book: not implemented");
        }

        static void ListBooks()
        {
            Console.WriteLine("List books: not implemented");
        }

        static void AddAuthor()
        {
            Console.WriteLine("Add author: not implemented");
        }

        static void ListAuthors()
        {
            Console.WriteLine("List authors: not implemented");
        }

        static void ShowHelp()
        {
            Console.WriteLine("Show help: not implemented");
        }
    }
}
