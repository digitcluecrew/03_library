﻿using System;
using System.Collections.Generic;

namespace library
{
    public class Author
    {
        public string Name
        {
            get;
            set;
        }
        public string Surname
        {
            get;
            set;
        }
        public int Age
        {
            get;
            set;
        }
        public BooksList Publications
        {
            get;
            set;
        }
        public Author(string name, string surname, int age)
        {
            Name = name;
            Surname = surname;
            Age = age;

            Publications = new BooksList();
        }
    }
}
